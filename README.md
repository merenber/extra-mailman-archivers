# Extra Mailman Archivers

This contains some extra archivers to be used with Mailman 3.
Currently there are two archivers, `MonthlyArchiver`, and `Pipermail`.

## Installation
Make sure to have the following packages installed first:
```sh
apt install python3-pip python3-setuptools python3-wheel
```

Then:
```sh
pip3 install .
```

## MonthlyArchiver
This archiver stores each message in a maildir folder for the message's year
and month.

The motivation behind `MonthlyArchiver` was to avoid storing thousands of
messages in a single directory, which the `Prototype` archiver currently
does.

Example directory structure:
```
/var/lib/mailman3/archives/
|-- monthly_archiver/
    |-- mylist@mydomain.com/
        |-- 2021/
            |-- April/
            |   |-- cur/
            |   |-- new/
            |   |   |-- 1618032020.M509265P87123Q1.mail
            |   |   |-- 1618032509.M464173P87208Q1.mail
            |   |-- tmp/
            |-- May/
                |-- cur/
                |-- new/
                |-- tmp/
```

To enable this archiver, add the following to your mailman.cfg:
```
[archiver.monthly_archiver]
class: extra_mailman_archivers.monthly_archiver.MonthlyArchiver
enable: yes
```

## Pipermail
This archiver attempts to add messages to Pipermail, the archiver for
Mailman 2. An existing Mailman 2 installation is required.

If the mailing list does not exist in the Mailman 2 directory, you
must create it first. Use the `newlist-pipermail` script in the `pipermail`
directory after copying it to the `bin` directory where Mailman 2
is installed. It works the same way as the traditional `newlist` script
except that it does not communicate with the MTA. **Note**: by default,
this will create a public archive; to make it private, unlink the appropriate
folder in the `archives/public` directory where Mailman 2 is installed.

To enable this archiver, add the following to your mailman.cfg:
```
[archiver.pipermail]
class: extra_mailman_archivers.pipermail.Pipermail
enable: yes
```

The default list URL for this archiver is `http://$domain/pipermail/$short_listname`
(Mailman adds a `List-Archive` header to each message with this value).
The default Mailman 2 directory is `/var/lib/mailman`.
To change either of these values, add the following to the pipermail
section in mailman.cfg:
```
configuration: /etc/mailman3/mailman-pipermail.cfg
```

Then, in `/etc/mailman3/mailman-pipermail.cfg`, set `base_url` and/or `mailman2_dir`
appropriately, e.g.
```
[general]
base_url: http://$domain/old/pipermail/$short_listname
```

To hide the list URL from each message, simply set `base_url` to an empty string:
```
base_url:
```
